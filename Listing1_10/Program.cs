﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Agregando una continuacion
 * la continuacion tiene varias opciones por ejemplo cancelada, fallada, completada
 */
namespace Listing1_10
{
    class Program
    {
        static void Main(string[] args)
        {
            Task<int> t = Task.Run(() => {
                return 10;
            });

            t.ContinueWith((s)=> {
                Console.WriteLine("Cancelado");
            },TaskContinuationOptions.OnlyOnCanceled);

            t.ContinueWith((s) => {
                Console.WriteLine("Faulted");
            }, TaskContinuationOptions.OnlyOnFaulted);

            var tarea = t.ContinueWith((s) => {
                Console.WriteLine("Completado");
            }, TaskContinuationOptions.OnlyOnRanToCompletion);

            tarea.Wait();

            Console.WriteLine("El resultado es: {0}", t.Result);
            Console.ReadKey();
        }
    }
}
