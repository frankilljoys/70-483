﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//usando el atributo ThreadStaticAttribute
/*
 * un hilo tiene su propia pila de llamadas que almacena todos los metodos que son ejecutados
 * variables locales son almacenadas en la pila de llamadas y son privadas al hilo
 * 
 * un hilo tambien puede tener sus propios datos que no son una variable local
 * marcando un campo con el atributo ThreadStatic, cada hilo tiene su propia copa de un campo
 * 
 * Se puede observar en este ejemplo que aunque la variable estatica es modificada en el 
 * hilo A, cuando la misma variable es usada en el metodo B, los cambios que recibió en el
 * metodo A no se ven reflejados y en ambos métodos el valor inicial de la variable es 0
 * 
 * Si se remueve el atributo ThreadStatic si se reflejarian los cambios realizados en un hilo y otro
 */
namespace Listing1_5
{
    class Program
    {
        [ThreadStatic]
        public static int campo_estatico;

        static void Main(string[] args)
        {
            new Thread(()=>{
                for (int i = 0; i < 10; i++)
                {
                    campo_estatico++;
                    Console.WriteLine("Hilo A: {0}", campo_estatico);
                }
            }).Start();

            new Thread(() => {
                for (int i = 0; i < 10; i++)
                {
                    campo_estatico++;
                    Console.WriteLine("Hilo B: {0}", campo_estatico);
                }
            }).Start();

            Console.WriteLine("Presione una tecla para salir");
            Console.ReadKey();
        }
    }
}
