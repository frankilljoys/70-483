﻿using System;

/*
 * Entendiendo delegados
 */
namespace Listing1_75
{
    class Program
    {
        public delegate int Calcular(int x, int y);
        public static int sumar(int x, int y)
        {
            return x + y;
        }

        public static int multiplicar(int x, int y)
        {
            return x * y;
        }
        static void Main(string[] args)
        {
            Calcular c = sumar;
            Console.WriteLine(c(3,4));

            c = multiplicar;
            Console.WriteLine(c(3,4));

            Console.ReadLine();
        }
    }
}
