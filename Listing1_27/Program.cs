﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Listing1_27
{
    class Program
    {
        static void Main(string[] args)
        {
            var numeros = Enumerable.Range(0, 20);

            try
            {
                var resultado = numeros.AsParallel().Where(a => par(a));
                resultado.ForAll(a => Console.WriteLine(a));
            }
            catch (AggregateException ae)
            {
                Console.WriteLine("Ocurrieron {0} excepciones", ae.InnerExceptions.Count);
            }

            Console.ReadLine();
        }

        static bool par(int i)
        {
            if (i % 10 == 0) throw new ArgumentException("i");
            return i % 2 == 0;
        }
    }
}
