﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * el metodo ForAll quita cualquier orden de ordenamiento que se le haya especificado antes
 */
namespace Listing1_26
{
    class Program
    {
        static void Main(string[] args)
        {
            var numero = Enumerable.Range(0, 10);
            var resultado = numero.AsParallel().Where(a => a % 3 == 0);
            resultado.ForAll(a => Console.WriteLine(a));

            Console.Read();
        }

    }
}
