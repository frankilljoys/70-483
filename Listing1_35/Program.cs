﻿using System;
using System.Threading.Tasks;

/*
 * Aca se muestra un problema con la concurrencia en el acceso a los datos
 * ya que son modificados en tiempos diferentes
 */
namespace Listing1_35
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 0;
            var arriba = Task.Run(() =>
            {
                for (int i = 0; i < 1000000; i++)
                {
                    n++;
                }
            });
            
                for (int i = 0; i < 1000000; i++)
                {
                    n--;
                }


            arriba.Wait();
            Console.WriteLine(n);

            Console.ReadLine();
        }
    }
}
