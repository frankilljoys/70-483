﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

/*
 * listing 1.18 uso de async y await
 */
namespace Listing1_18
{
    class Program
    {
        static void Main(string[] args)
        {
            string result = DescargarContenido().Result;
            Console.Write(result);
            Console.ReadKey();
        }

        public static async Task<string> DescargarContenido()
        {
            using (HttpClient http = new HttpClient())
            {
                string result = await http.GetStringAsync("http://www.microsoft.com");
                return result;
            }
        }
    }
}
