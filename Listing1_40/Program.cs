﻿using System;
using System.Threading;
using System.Threading.Tasks;

/*
 * con Interlock se evita el problema de lock, ya que
 */
namespace Listing1_40
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 0;
            var arriba = Task.Run(() =>
            {
                for (int i = 0; i < 1000000; i++)
                {
                    Interlocked.Increment(ref n);
                }
            });

            var abajo = Task.Run(() =>
            {
                for (int i = 0; i < 1000000; i++)
                {
                    Interlocked.Decrement(ref n);
                }
            });
            

            arriba.Wait();
            abajo.Wait();
            Console.WriteLine(n);
            Console.ReadLine();
        }
    }
}
