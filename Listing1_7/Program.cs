﻿using System;
using System.Threading;

/*
 * Grupo de Hilos o thread pools
 * una cosa a ser conciente es que como los hilos son reusados,
 * ellos tambien reusan sus estados locales.
 * no puedes confiar en el estado que puede potencialmente ser compartido entre multiples 
 * operaciones
 */
namespace Listing1_7
{
    class Program
    {
        static void Main(string[] args)
        {
            ThreadPool.QueueUserWorkItem((s) => {
                Console.WriteLine("Trabajando en un hilo desde threadpool");
            });

            Console.ReadKey();
        }
    }
}
