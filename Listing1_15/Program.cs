﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//Esperar a que cualquier tarea se complete
namespace Listing1_15
{
    class Program
    {
        static void Main(string[] args)
        {
            Task<int>[] tareas = new Task<int>[3];
            tareas[0] = Task.Run(() =>
            {
                Thread.Sleep(2000);
                return 1;
            });

            tareas[1] = Task.Run(() =>
            {
                Thread.Sleep(1000);
                return 2;
            });

            tareas[2] = Task.Run(() =>
            {
                Thread.Sleep(3000);
                return 3;
            });

            while (tareas.Length > 0)
            {
                int i = Task.WaitAny(tareas);
                Task<int> tareasCompletadas = tareas[i];
                Console.WriteLine(tareasCompletadas.Result);

                var temp = tareas.ToList();
                temp.RemoveAt(i);
                tareas = temp.ToArray();
            }
            
            Console.ReadKey();
        }
    }
}