﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//llamar hilo y pasarle un parametro
namespace Listing1_3
{
    class Program
    {
        public static void MetodoHilo(object o)
        {
            for (int i = 0; i < (int)o; i++)
            {
                Console.WriteLine("Desde el metodo MetodoHilo: {0}", i);
                Thread.Sleep(0);
            }
        }
        static void Main(string[] args)
        {
            Thread t = new Thread(new ParameterizedThreadStart(MetodoHilo));
            t.Start(5); // Este es el parametro que recibira el metodo

            t.Join();
            Console.ReadLine();
        }
    }
}
