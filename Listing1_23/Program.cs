﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * query paralelo no ordenado
 */
namespace Listing1_23
{
    class Program
    {
        static void Main(string[] args)
        {
            var numeros = Enumerable.Range(0, 10);
            var resultado = numeros.AsParallel().Where(a => a % 3 == 0).ToArray();
            foreach (var item in resultado)
            {
                Console.WriteLine(item);
            }
            Console.Read();
        }
    }
}
