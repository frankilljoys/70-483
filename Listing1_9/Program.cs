﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Usando una tarea que devuelve un valor
 */
namespace Listing1_9
{
    class Program
    {
        static void Main(string[] args)
        {
            Task<int> valor = Task.Run(() =>
            {
                return 15;
            });

            Task<int> valor2 = Task.Run(() =>
            {
                return devolverValor();
            });
            Console.WriteLine("El valor devuelto es: {0}", valor.Result);
            Console.WriteLine("El valor2 devuelto es: {0}", valor2.Result);
            Console.ReadKey();
        }

        static int devolverValor()
        {
            return 35;
        }
    }
}
