﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//Creando un hilo con la clase Thread
//Ambos hilos tienen la misma prioridad

//Con Thread.Sleep(0); se le dice que el hilo ha terminado, asi no se tiene que esperar
//por el tiempo que se le asigno al hilo a que se agote, sino que se cambia inmediatamente a otro hilo
namespace Capitulo1.Listing1_1
{
    class Program
    {
        public static void MetodoHilo()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Desde el metodo MetodoHilo: {0}",i);
                Thread.Sleep(0);
            }
        }
        static void Main(string[] args)
        {
            Thread t = new Thread(new ThreadStart(MetodoHilo));
            t.Start();

            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine("Desde el main: {0}", i);
                Thread.Sleep(0);
            }

            t.Join();
            Console.ReadLine();
        }
    }
}
