﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Con secuencial le decimos que al usar el metodo take no rompa el orden que necesitamos
namespace Listing1_25
{
    class Program
    {
        static void Main(string[] args)
        {
            var numeros = Enumerable.Range(0, 10);
            var resultado = numeros.AsParallel().AsOrdered().Where(a => a % 2 == 0).AsSequential();
            foreach (var i in resultado.Take(3))
            {
                Console.WriteLine(i);
            }
            Console.Read();
        }
    }
}
