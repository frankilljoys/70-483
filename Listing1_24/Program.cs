﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Si se le coloca AsOrdered al paralelo si respeta el ordenamiento, caso contrario no
 */
namespace Listing1_24
{
    class Program
    {
        static void Main(string[] args)
        {
            var numeros = Enumerable.Range(0, 10);
            var resultado = numeros.AsParallel().Where(a => a % 3 == 0).ToArray();
            var resultado2 = numeros.AsParallel().AsOrdered().Where(a => a % 3 == 0).ToArray();
            foreach (var i in resultado)
            {
                Console.WriteLine(i);
            }
            Console.WriteLine("--------------------------");
            foreach (var i in resultado2)
            {
                Console.WriteLine(i);
            }
            Console.ReadKey();
        }
    }
}
