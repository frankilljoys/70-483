﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace Listing1_28
{
    class Program
    {
        static void Main(string[] args)
        {
            BlockingCollection<string> bc = new BlockingCollection<string>();
            Task leer = Task.Run(() =>
            {
                while (true)
                {
                    Console.WriteLine("Ingresaste {0}",bc.Take());
                }
            });

            Task escribir = Task.Run(() =>
            {
                while (true)
                {
                    string cadena = Console.ReadLine();
                    if (String.IsNullOrEmpty(cadena)) break;
                    bc.Add(cadena);
                }
            });

            escribir.Wait();
        }
    }
}
