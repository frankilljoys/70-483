﻿using System;
using System.Windows.Forms;

namespace CovarianzaContravarianza
{
    public partial class Form1 : Form
    {
        //Un delegado que retorna una persona
        private delegate Persona ReturnPersonaDelegado();
        private ReturnPersonaDelegado ReturnPersonaMetodo;

        //un metodo que retorna un empleado
        private Empleado ReturnEmpleado()
        {
            return new Empleado();
        }

        //un delegado que toma como parametro un empleado
        private delegate void EmpleadoParametroDelegado(Empleado empleado);
        EmpleadoParametroDelegado empleadoParametroDelegado; 

        //un metodo que toma una persona como parametro
        private void PersonaParametro(Persona persona)
        {
            
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //uso de covarianza para asignar un metodo que devuelve empleado a un delegado que devuelve persona
            ReturnPersonaMetodo = ReturnEmpleado;

            //uso de contravarianza para asignar un metodo que recibe un objeto Persona a un delegado que recibe un objeto Empleado
            empleadoParametroDelegado = PersonaParametro;
        }
    }
}
