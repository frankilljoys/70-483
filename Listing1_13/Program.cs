﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Usando TaskFactory
 */
namespace Listing1_13
{
    class Program
    {
        static void Main(string[] args)
        {
            Task<Int32[]> padre = Task.Run(() =>
            {
                TaskFactory tf = new TaskFactory(TaskCreationOptions.AttachedToParent,TaskContinuationOptions.ExecuteSynchronously);
                var resultado = new Int32[3];
                tf.StartNew(() => resultado[0] = 0);
                tf.StartNew(() => resultado[1] = 1);
                tf.StartNew(() => resultado[2] = 2);
                return resultado;
            });

            var hiloFinal = padre.ContinueWith(tarePadre =>
            {
                foreach (int i in tarePadre.Result)
                {
                    Console.WriteLine("Resultado: {0}", i);
                }
            });

            hiloFinal.Wait();
            Console.ReadKey();
        }
    }
}
