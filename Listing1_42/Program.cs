﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

/*
 * usando un token de cancelacion para cancelar tareas
 */
namespace Listing1_42
{
    class Program
    {
        static void Main(string[] args)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken token = cancellationTokenSource.Token;

            Task t = Task.Run(() =>
            {
                while (!token.IsCancellationRequested)
                {
                    Console.WriteLine("*");
                    Thread.Sleep(1000);
                }
                Console.WriteLine("Tarea cancelada");
            },token);

            Console.WriteLine("Presione ENTER para cancelar la tarea");
            Console.ReadLine();
            cancellationTokenSource.Cancel();

            Console.WriteLine("Presione ENTER para salir");
            Console.Read();
        }
    }
}
