﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Capitulo7
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = Stopwatch.StartNew();
            //CorrerSecuencial();
            //CorrerConHilos();
            CorrerEnThreadPool();

            Console.WriteLine("Completado en {0}ms", sw.ElapsedMilliseconds);
            if (Debugger.IsAttached)
            {
                Console.WriteLine("Presione una tecla para continuar");
                Console.ReadKey(true);
            }
        }

        static void CorrerSecuencial()
        {
            double resultado = 0d;

            resultado += leerDatosDesdeIO();
            resultado += hacerCalculoIntensivo();

            Console.WriteLine("El resultado es {0}", resultado);
        }

        static void CorrerConHilos()
        {
            double resultado = 0d;
            double resultado2 = 0d;

            var hilo = new Thread(() => { resultado += leerDatosDesdeIO(); });
            hilo.Start();

            resultado2 += hacerCalculoIntensivo();
            hilo.Join();

            resultado += resultado2;

            Console.WriteLine("El resultado es {0}", resultado);
        }

        static void CorrerEnThreadPool()
        {
            double resultado = 0d;

            ThreadPool.QueueUserWorkItem((s) => resultado += leerDatosDesdeIO());
            
            resultado += hacerCalculoIntensivo();

            Console.WriteLine("El resultado es {0}", resultado);
        }

        static double leerDatosDesdeIO()
        {
            Thread.Sleep(5000);
            return 15d;
        }

        static double hacerCalculoIntensivo()
        {
            double result = 100000000d;

            for (int i = 1; i < int.MaxValue; i++)
            {
                result /= i;
            }
            return result + 10d;
        }
    }
}
