﻿using System;
using System.Threading;

namespace Listing1_6
{
    class Program
    {
        public static ThreadLocal<int> campo_local = new ThreadLocal<int>(() => {
            return Thread.CurrentThread.ManagedThreadId;
        });
        static void Main(string[] args)
        {
            new Thread(() => {
                for (int i = 0; i < campo_local.Value; i++)
                {
                    Console.WriteLine("Hilo A: {0}", i);
                }
            }).Start();

            new Thread(() => {
                for (int i = 0; i < campo_local.Value; i++)
                {
                    Console.WriteLine("Hilo B: {0}", i);
                }
            }).Start();
            Console.ReadKey();
        }
    }
}
