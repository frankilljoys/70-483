﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Listing1_29
{
    class Program
    {
        static void Main(string[] args)
        {
            BlockingCollection<string> bc = new BlockingCollection<string>();
            Task leer = Task.Run(() =>
            {
                foreach (string v in bc.GetConsumingEnumerable())
                {
                    Console.WriteLine("Ingresaste {0}",v);
                }
            });

            Task escribir = Task.Run(() =>
            {
                while (true)
                {
                    string cadena = Console.ReadLine();
                    if (string.IsNullOrEmpty(cadena)) break;
                    bc.Add(cadena);
                }
            });

            escribir.Wait();
        }
    }
}
