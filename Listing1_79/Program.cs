﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * expresion lambda para delegados
 */
namespace Listing1_79
{
    class Program
    {
        public delegate int Calcular(int x, int y);
        static void Main(string[] args)
        {
            Calcular c = (x, y) => x + y;
            Console.WriteLine(c(3,4));
            c = (x, y) => x * y;
            Console.WriteLine(c(3,4));

            Console.Read();
        }
    }
}
