﻿using System.Threading;
using System.Threading.Tasks;

namespace Listing1_19
{
    class Program
    {
        static void Main(string[] args)
        {
        }

        public Task DormirAsincronoA(int milisegundos)
        {
            return Task.Run(() => {
                Thread.Sleep(milisegundos);
            });
        }

        public Task DormirAsincronoB(int milisegundos)
        {
            TaskCompletionSource<bool> tsc = null;
            var t = new Timer(delegate { tsc.TrySetResult(true); }, null, -1, -1);
            tsc = new TaskCompletionSource<bool>(t);
            t.Change(milisegundos, -1);
            return tsc.Task;
        }
    }
}
