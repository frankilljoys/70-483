﻿using System;
using System.Threading.Tasks;

//Uso de parallele for, foreach
namespace Listing1_16
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] datos = new int[20];
            for (int i = 0; i < 20; i++)
            {
                datos[i] = i + 1;
            }
            ParallelLoopResult loopResult = Parallel.For(0, 1000, (int i, ParallelLoopState loopState) => {
                if (i == 500)
                {
                    Console.WriteLine("Saliendo del loop");
                    loopState.Break();
                }
                return;
            });

            ParallelLoopResult cicloParalelo = Parallel.For(0, datos.Length, (int i, ParallelLoopState loopState) => {
                Console.WriteLine(datos[i]);
                return;
            });

            Console.WriteLine("-------------------------------\n");
            ParallelLoopResult cicloParalelo2 = Parallel.ForEach(datos, (datoActual) =>
             {
                 Console.WriteLine(datoActual);
                 return;
             });

            Console.ReadKey();
        }
    }
}
