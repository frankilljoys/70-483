﻿using System;
using System.Threading.Tasks;

/*
 * tener cuidado con el uso de lock porque se puede dar que ambos hilos se queden esperando a que el otro finalice
 */
namespace Listing1_36
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 0;
            object _lock = new object();
            var arriba = Task.Run(() =>
            {
                for (int i = 0; i < 1000000; i++)
                {
                    lock (_lock)
                    n++;
                }
            });

            for (int i = 0; i < 1000000; i++)
            {
                lock(_lock)
                n--;
            }

            arriba.Wait();
            Console.WriteLine(n);
            Console.ReadLine();
        }
    }
}
