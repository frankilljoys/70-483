﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//usar parallel no garantiza ningun orden
namespace Listing1_22
{
    class Program
    {
        static void Main(string[] args)
        {
            var numeros = Enumerable.Range(0, 100000000);
            var resultadoParalelo = numeros.AsParallel().Where(a => a % 2 == 0).ToArray();
        }
    }
}
