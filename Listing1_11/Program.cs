﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Adjuntando tareas hijas a tareas padres
 * 
 * la tareafinal solo corre cuando la tarea padre ha finalizado
 * la tarea padre finaliza cuando los tres hijos han finalizado
 */
namespace Listing1_11
{
    class Program
    {
        static void Main(string[] args)
        {
            Task<Int32[]> padre = Task.Run(() =>
               {
                   var resultados = new Int32[3];
                   new Task(() => resultados[0] = 0, TaskCreationOptions.AttachedToParent).Start();
                   new Task(() => resultados[1] = 1, TaskCreationOptions.AttachedToParent).Start();
                   new Task(() => resultados[2] = 2, TaskCreationOptions.AttachedToParent).Start();
                   return resultados;
               });

            var tareaFinal = padre.ContinueWith(tareaPadre =>
            {
                foreach (int i in tareaPadre.Result) 
                {
                    Console.WriteLine("Resultado: {0}",i);
                }
            });

            tareaFinal.Wait();
            Console.ReadKey();
        }
    }
}
