﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//Usando un hilo de segundo plano
namespace Capitulo1.Listing1_2
{
    class Program
    {
        public static void MetodoHilo()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Desde el metodo MetodoHilo: {0}", i);
                Thread.Sleep(0);
            }
        }
        static void Main(string[] args)
        {
            Thread t = new Thread(new ThreadStart(MetodoHilo));
            t.IsBackground = true;
            t.Start();

            Console.ReadLine();
        }
    }
}
