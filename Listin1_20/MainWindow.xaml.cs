﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Listin1_20
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            HttpClient http = new HttpClient();
            /*
             * Si se pone configure await como falso, se deshabilita la sincronizacion de contexto
             * y por consiguiente se genera error al querer modificar la UI
             */
            //string contenido = await http.GetStringAsync("http://www.google.com").ConfigureAwait(false);
            string contenido = await http.GetStringAsync("http://www.google.com");
            lbl.Content = contenido;
        }
    }
}
