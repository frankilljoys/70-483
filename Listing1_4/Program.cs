﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Listing1_4
{
    class Program
    {
        static void Main(string[] args)
        {
            bool detener = false;
            Thread hilo = new Thread(
                new ThreadStart(()=>
                {
                    while (!detener)
                    {
                        Console.WriteLine("Corriendo hilo....!");
                        Thread.Sleep(1000);
                    }

                    if (detener)
                    {
                        Console.WriteLine("\nEl hilo fue detenido..");
                        Console.WriteLine("Presione una tecla para salir");
                        Console.ReadKey();
                    }
                })
             );

            hilo.Start();

            Console.WriteLine("Presione una tecla finalizar el hilo");
            Console.ReadKey();

            detener = true;
            hilo.Join();
        }
    }
}
